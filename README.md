# Reflectometer Summary
This device is a 10 wavelength colorimeter with ten LEDs ranging between 365nm to 940nm, and two pin photodiode detectors with ranges between 350nm to 700nm, and 700nm to 1000nm respectively.  We use two separate detectors because shining visible light on samples containing chlorophyll will produce fluorescence in the 700 - 800nm range which will confound the reflectance signals.  By having two separate detectors, we can separate the reflectance from the fluorescence.  In addition, the LEDs are pulsed in order to save energy and lower heat accumulation (which produces noise) and allows for brighter flashes and therefore greater signal. 

# Applications and Description
Applications for the reflectometer include chlorophyll content in leaves, estimating total organic carbon in soil, food quality estimates, and general scientific applications.  The hardware is designed to be used in the field or in the lab, specifically with flat samples (like leaves), samples in cuvettes (for lab applications, loose solids in a glass pietre dish (like soils).  While bulk solids (like fruits) is possible the design is less optimal for measuring the surface of a large solid object..  Software subtraction of ambient light is used to deal with light contamination on the detectors.  

The circuit has an on-board lithium battery charger (designed for a 3.7V 18650 Li battery) and current controlled pulsed LEDs to illuinate the sample, as well as an optional hall effect sensor, contactless temperature sensor and temp/pressure/rh sensor (BME280).

In addition to the 10 wavelength colorimeter circuit, there is an optional circuit to control two Hamamatsu MEMS spectrometers which measure from 1350 - 1950nm (C13272-02 and C14272).  This circuit is designed for lab use only, and there is no on-board light supplied.  Normally, this circuit is not populated.  You can identify these and other optional parts of the circuit in the "datasheet" field of the schematic.  

# Setup
Download or clone the repository on your computer.  The "Main" folder contains the most current version of the schematic, layout, and BOM.  This device has a ARM MK20 MCU and a teensy boot loader which is connected via a 10 pin connector (see "programmer" folder for that circuit).  You need to also build the bootloader to load the firmware.  

Files were created in KiCAD.  Use with the most recent nightly build of KiCAD.  Add the multispeq.pretty folder to your list of libraries and put it on the top of the list - this is important because some components names overlap with components in other libraries and the multispeq.pretty versions will only be used if it is placed at the top of the library list.  All other libraries which are available during KiCAD installation should also be installed.

All parts should have manufacturer part number references, and you can use a BOM tool (I use octoparts BOM tool) to build the BOM and order the items.  Some items are specialty order and not available through octopart (like the Hamamatsu detectors).  If you want to export the BOM, follow the directions to install the BOM exporting tool (I use this one - https://forum.kicad.info/t/kicad-bom-wizard-plugin-with-customisable-output-can-make-html-and-csv-bom/2142, but there is also BOMsaway and others).  

# Additional files

To build this devices from scratch, you will also need:

* **Firmware**: https://gitlab.com/our-sci/reflectance-spec-firmware
* **CAD / Physical design**: https://cad.onshape.com/documents/849be056da41993fee5440bf/w/5ea0968e39e13e6e3de3a5ba/e/f5bcf0fa9d4cdfc9de388d42
* **Bill of Materials** (everything but the PCB): https://docs.google.com/spreadsheets/d/1d4Xplr8sztEgrEis-LiSqLnN9V1eaN1zSc19izoaaig/edit?usp=sharing
